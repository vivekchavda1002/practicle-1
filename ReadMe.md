
# Sample Angular Project
This is Demo Practicle - 1  For Basic Understanding of Angular Project Directory Structure and CLI.

### Basic Information about Practicle-1
Created New Project with Command **ng new ng-demo-app**

Created service with command **ng g s sampleService**

Created new Module with command **ng g m sample-module**

Created new component with command **ng g c sample**

For Practice Installled Third Party Module *@ng-bootstrap/ng-bootstrap*

### Installation
Clone Project with link https://vivekchavda1002@bitbucket.org/vivekchavda1002/practicle-1.git

Once All files are cloned Install the require packages

```bash
  npm install
  ng serve
```

####   Server will be live on https://localhost:4200